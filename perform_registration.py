# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 13:50:17 2016

@author: Geert
"""

import os
import shutil
import multiresolutionimageinterface as mir
import matplotlib as mpl
import matplotlib.pyplot as plt
import logging
import numpy as np
from skimage.transform import rotate
from skimage.measure import label
from scipy import ndimage
import SimpleITK as sitk
import subprocess
import tempfile

def perform_registration(fixed_pth, moving_pth, out_pth, reg_levels = [], mask_threshold = 0.18, nr_of_largest_components_to_use=-1, tm_step_sizes=[20,20,20], debug=False):
    logger = logging.getLogger('perform_registration')
    bin_cmap = mpl.colors.LinearSegmentedColormap.from_list('bin_map',[[0,0,0,0],[0,1,0,1]],2)
    try: 
        subprocess.check_output(["elastix.exe"])
    except :
        logging.error("Elastix does not exist, cannot perform registration")
        return
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.WARNING)
    r = mir.MultiResolutionImageReader()
    f_img = r.open(fixed_pth)
    m_img = r.open(moving_pth)
    f_levels = f_img.getNumberOfLevels()
    m_levels = m_img.getNumberOfLevels()
    f_spacing = f_img.getSpacing()
    m_spacing = m_img.getSpacing()
    if not f_spacing or m_spacing:
        logger.warning("Spacing is not specified in fixed or moving images, cannot check for equality of scanning magnification.")
    else:
        if (f_spacing != m_spacing):
            logger.warning("Spacing at level 0 of fixed (" + str(f_spacing[0])+ ") and moving image (" + str(m_spacing[0])+ ") is not the same, might be scanned at different magnifications.")    
    logger.debug("Fixed image has " + str(f_levels) + " levels")
    logger.debug("Moving image has " + str(m_levels) + " levels.")
    logger.debug("Fixed image has a spacing of (" + ", ".join([str(x) for x in f_spacing]) + ").")
    logger.debug("Moving image has a spacing of (" + ", ".join([str(x) for x in m_spacing]) + ").")
    if not reg_levels:
        reg_levels = [f_levels - 1]*2 if f_levels < m_levels else [m_levels - 1]*2
    logger.debug("Using levels " + " and ".join([str(x) for x in reg_levels]) + " of fixed and moving image, respectively, for registration.")
    # Preprocess images    
    f_dims = f_img.getLevelDimensions(reg_levels[0])
    m_dims = m_img.getLevelDimensions(reg_levels[1])
    f_data = f_img.getUCharPatch(0, 0, f_dims[0], f_dims[1], reg_levels[0])
    m_data = m_img.getUCharPatch(0, 0, m_dims[0], m_dims[1], reg_levels[1])
    logger.debug("Fixed image has dimensions of (" + ", ".join([str(x) for x in f_dims]) + ") at registration level.")
    logger.debug("Moving image has dimensions of (" + ", ".join([str(x) for x in m_dims]) + ") at registration level.")
    f_data_grey = (255 - f_data.mean(axis=2)) / 255.
    m_data_grey = (255 - m_data.mean(axis=2)) / 255.
    if debug:
        plt.figure()
        plt.subplot(2, 2, 1)
        plt.imshow(f_data)
        plt.title("Fixed image in color")
        plt.subplot(2, 2, 2)
        plt.imshow(m_data)
        plt.title("Moving image in color")
        plt.subplot(2, 2, 3)
        plt.imshow(f_data_grey, cmap='gray')
        plt.title("Fixed image in inverted gray scale")
        plt.subplot(2, 2, 4)
        plt.imshow(m_data_grey, cmap='gray')
        plt.title("Moving image in inverted gray scale")
        f = plt.gcf()
        f.canvas.set_window_title('Images before registration.')
        for ax in f.axes:
            ax.set_yticks([])
            ax.set_xticks([])
        plt.tight_layout()
    # Perform template matching alignment
    f_msk = calculate_tissue_mask(f_data, mask_threshold, mask_threshold, mask_threshold, mask_threshold)
    m_msk = calculate_tissue_mask(m_data, mask_threshold, mask_threshold, mask_threshold, mask_threshold)
    mask_subplot_rows = 1
    if nr_of_largest_components_to_use > 0:
        mask_subplot_rows = 2
    if debug:
        plt.figure()
        plt.subplot(mask_subplot_rows, 2, 1)
        plt.imshow(f_data_grey, cmap='gray')
        plt.hold(True)
        plt.imshow(f_msk, cmap=bin_cmap, alpha = 0.5)
        plt.title("Fixed image with \n tissue mask overlayed")
        plt.subplot(mask_subplot_rows, 2, 2)
        plt.imshow(m_data_grey, cmap='gray')
        plt.hold(True)
        plt.imshow(m_msk, cmap=bin_cmap, alpha = 0.5)        
        plt.title("Moving image with \n tissue mask overlayed")
    if nr_of_largest_components_to_use > 0:
        f_msk = extract_largest_component(f_msk, nr_of_largest_components_to_use)
        m_msk = extract_largest_component(m_msk, nr_of_largest_components_to_use)
        if debug:
            plt.subplot(2, 2, 3)
            plt.imshow(f_data_grey, cmap='gray')
            plt.hold(True)
            plt.imshow(f_msk, cmap=bin_cmap, alpha = 0.5)            
            plt.title("Fixed image with \n selected components overlayed.")
            plt.subplot(2, 2, 4)
            plt.imshow(m_data_grey, cmap='gray')
            plt.hold(True)
            plt.imshow(m_msk, cmap=bin_cmap, alpha = 0.5)        
            plt.title("Moving image with \n selected components overlayed.")
    if debug :
        f = plt.gcf()
        f.canvas.set_window_title('Images with tissue masks')
        for ax in f.axes:
            ax.set_yticks([])
            ax.set_xticks([])
        plt.tight_layout()            
    tx, ty, ta, t_img = template_matching(f_data_grey*f_msk, m_data_grey*m_msk, tm_step_sizes)
    if debug:
        f = plt.figure()
        plt.imshow(m_data_grey, cmap='gray')
        plt.hold(True)
        cmap = plt.cm.get_cmap('hot')
        cmap.set_bad(alpha=0)
        t_img[t_img<0.001] = np.nan
        plt.imshow(t_img, alpha = 0.5, cmap=cmap)     
        for ax in f.axes:
            ax.set_yticks([])
            ax.set_xticks([])
        plt.tight_layout()
        f.canvas.set_window_title('Result after template matching')    
    logger.debug("Initial transform is (tx=" + str(tx) + ", ty=" + str(ty) + ", angle=" + str(ta) + ")")
    # Fill initial transform for elastix
    f = open(os.path.join(os.path.split(__file__)[0], "Parameters", "Parameters_Euler.txt"), "r")
    prms = f.readlines()
    f.close()
    prms[2]  = prms[2]  % {'transform_params' : str(-np.deg2rad(ta)) + " " + str(-1*tx) + " " + str(-1*ty)}
    prms[11] = prms[11] % {'image_size' : str(f_dims[0]) + " " + str(f_dims[1])}
    prms[19] = prms[19] % {'image_center' : str(f_dims[0]/2) + " " + str(f_dims[1]/2)}
    tmp_dir = tempfile.mkdtemp("wsiregistration")
    tmp_fl  = os.path.join(tmp_dir, "Parameters_Euler_filled.txt")
    f = open(tmp_fl, "w")
    f.write("".join(prms))
    f.close()
    # Write out images and run Elastix
    f_itk_img = sitk.GetImageFromArray(f_data_grey*f_msk)
    m_itk_img = sitk.GetImageFromArray(m_data_grey*m_msk)
    sitk.WriteImage(f_itk_img, os.path.join(tmp_dir, "fixed.mhd"))
    sitk.WriteImage(m_itk_img, os.path.join(tmp_dir, "moving.mhd"))
    elastix = subprocess.Popen(["elastix.exe", "-f", os.path.join(tmp_dir, "fixed.mhd"),
                                "-m", os.path.join(tmp_dir, "moving.mhd"),
                                "-p", os.path.join(os.path.split(__file__)[0], "Parameters", "Parameters_Rigid.txt"),
                                "-t0", tmp_fl, "-out", tmp_dir], stdout=subprocess.PIPE)
    output = elastix.communicate()
    if elastix.returncode != 0:
        logging.error("Elastix command failed: ")
        logging.error(output[0])
        logging.error(output[1])
        return
    if debug:
        r_img = sitk.GetArrayFromImage(sitk.ReadImage(os.path.join(tmp_dir, "result.0.mhd")))
        f = plt.figure()
        plt.imshow(f_data_grey, cmap='gray')
        plt.hold(True)
        cmap = plt.cm.get_cmap('hot')
        cmap.set_bad(alpha=0)
        r_img[r_img<0.001] = np.nan
        plt.imshow(r_img, alpha = 0.5, cmap=cmap)     
        for ax in f.axes:
            ax.set_yticks([])
            ax.set_xticks([])
        plt.tight_layout()
        f.canvas.set_window_title('Result after final affine registration')            
    downsample = f_img.getLevelDownsample(reg_levels[0])
    correct_transform_for_downsample(tmp_fl, 1, downsample, f_img.getLevelDimensions(0))
    correct_transform_for_downsample(os.path.join(tmp_dir, "TransformParameters.0.txt"), 4, downsample, f_img.getLevelDimensions(0))
    if out_pth:
        shutil.copyfile(tmp_fl, os.path.join(out_pth, "Parameters_Euler_filled.txt"))
        shutil.copyfile(os.path.join(tmp_dir, "TransformParameters.0.txt"), os.path.join(out_pth, "TransformParameters.0.txt"))
    shutil.rmtree(tmp_dir)
      
def apply_transformation_to_slide(moving_img_pth, out_pth, ini_tr_pth, aff_tr_pth, out_level=0):
    r = mir.MultiResolutionImageReader()
    w = mir.MultiResolutionImageWriter()   
    moving_im = r.open(str(moving_img_pth))
    mov_dims = moving_im.getLevelDimensions(out_level)
    sx, sy = [int(x) for x in read_image_size_from_transform(ini_tr_pth)]
    downsample_out_level = 2 ** out_level
    dims_out = (sx / downsample_out_level, sy / downsample_out_level)
    ini_tr = list(read_transform(ini_tr_pth))
    aff_tr = list(read_transform(aff_tr_pth))
    ini_tr[1] /= downsample_out_level
    ini_tr[2] /= downsample_out_level
    ini_tr[3] /= downsample_out_level
    ini_tr[4] /= downsample_out_level
    aff_tr[4] /= downsample_out_level
    aff_tr[5] /= downsample_out_level
    aff_tr[6] /= downsample_out_level
    aff_tr[7] /= downsample_out_level
    t_mat = concatenate_transforms(ini_tr, aff_tr)
    xs = np.tile(range(512), 512)
    ys = np.repeat(range(512), 512)
    w.openFile(out_pth)
    w.setTileSize(512)
    w.setCompression(mir.LZW)
    w.setDataType(mir.UChar)
    w.setInterpolation(mir.Linear)
    w.setColorType(mir.RGB)
    w.writeImageInformation(dims_out[0], dims_out[1])
    zero_tile = np.zeros(512*512*3, dtype='ubyte')
    for y in range(0, dims_out[1], 512):
        print str(y) + "/" + str(dims_out[1])
        for x in range(0, dims_out[0], 512):
            coords = np.vstack((xs + x, ys + y, np.ones(xs.shape)))
            t_coords = np.dot(t_mat, coords)
            x_t_coords = t_coords[0]
            y_t_coords = t_coords[1]
            bbox = np.array([x_t_coords.min(), x_t_coords.max(), y_t_coords.min(), y_t_coords.max()])
            if (bbox[1] < 0) or (bbox[3] < 0) or bbox[0] >= mov_dims[0] or bbox[2] >= mov_dims[1]: # Need to improve this check
                w.writeBaseImagePart(zero_tile)
            else:
                x_t_coords -= np.floor(bbox[0])
                y_t_coords -= np.floor(bbox[2])
                p = moving_im.getUCharPatch(int(np.floor(bbox[0])*downsample_out_level), int(np.floor(bbox[2])*downsample_out_level), int(np.ceil(bbox[1]) - np.floor(bbox[0])), int(np.ceil(bbox[3]) - np.floor(bbox[2])), out_level)
                p_t = np.concatenate([ndimage.interpolation.map_coordinates(p[:,:,x], (y_t_coords.reshape(512,512), x_t_coords.reshape(512,512)), order=1, mode='nearest')[None] for x in range(3)]).transpose(1,2,0)
                w.writeBaseImagePart(p_t.flatten())
    w.finishImage()
    
def read_image_size_from_transform(pth):
    f = open(pth, 'r')
    lines = f.readlines()
    f.close()
    return [float(y) for y in [x for x in lines if "(Size" in x][0].split(')')[0].split(' ')[1:]]

def read_transform(pth):
    f = open(pth, 'r')
    lines = f.readlines()
    params = [float(y) for y in [x for x in lines if "(TransformParameters" in x][0].split(')')[0].split(' ')[1:]]
    rot_center = [float(y) for y in [x for x in lines if "(CenterOfRotationPoint" in x][0].split(')')[0].split(' ')[1:]]
    params.extend(rot_center)    
    f.close()
    return params
   
def concatenate_transforms(init, affine):
    it_rot = [[np.cos(init[0]), -np.sin(init[0]), 0], [np.sin(init[0]), np.cos(init[0]), 0],[0,0,1]]
    it_t = [[1, 0, init[1]], [0, 1, init[2]], [0,0,1]]
    it_sc = [[1, 0, -init[-2]],[0,1,-init[-1]], [0,0,1]]
    it_sb = [[1, 0, init[-2]],[0,1,init[-1]], [0,0,1]]
    at_at = np.array([[affine[0], affine[1], affine[4] ], [affine[2], affine[3], affine[5]], [0,0,1]], dtype='float32')
    at_sb = [[1, 0, affine[-2]],[0,1,affine[-1]], [0,0,1]]
    at_sc = [[1, 0, -affine[-2]],[0,1,-affine[-1]], [0,0,1]]
    return np.dot(at_sb, np.dot(at_at, np.dot(at_sc, np.dot(it_t, np.dot(np.dot(it_sb, it_rot), it_sc)))))    
    
def extract_largest_component(msk, nr_components):
    label_objects = label(msk, background=0) + 1
    sizes = np.bincount(label_objects.ravel())
    sizes[0] = 0
    indices = list(reversed(np.argsort(sizes)))[:nr_components]
    largest = np.in1d(label_objects, indices).reshape(label_objects.shape)
    return largest
    
def calculate_tissue_mask(in_img, thrR, thrG, thrB, thrA):
    logR = (in_img[:,:,0] > 0) * (-1*np.log(in_img[:,:,0]/255.))
    logG = (in_img[:,:,1] > 0) * (-1*np.log(in_img[:,:,1]/255.))
    logB = (in_img[:,:,2] > 0) * (-1*np.log(in_img[:,:,2]/255.))
    logA = (logR + logG + logB) / 3.
    mask = ((logR > thrR) * (logG > thrG) *
            (logB > thrB) * (logA > thrA)).astype('ubyte')
    return mask
    
def correct_transform_for_downsample(pth, start_param, downsample, reg_level_dims):
    f = open(pth, 'r')
    lines = f.readlines()
    param_line = -1
    center_line = -1
    initial_transform_line = -1
    size_line = -1
    for i, line in enumerate(lines):
        if "(TransformParameters" in line:           
            params = [float(y) for y in line.split(')')[0].split(' ')[1:]]
            param_line = i
        elif "(CenterOfRotationPoint" in line :
            center_line = i
        elif "(InitialTransformParametersFileName" in line:
            if not "NoInitialTransform" in line :
                initial_transform_line = i
        elif "(Size" in line :
            size_line = i
    for i in range(start_param, len(params)):
        params[i] *= downsample
    cx = reg_level_dims[0] / 2.
    cy = reg_level_dims[1] / 2.
    if param_line > 0:
        lines[param_line] = "(TransformParameters " + " ".join([str(p) for p in params]) + ")\n"
    if center_line > 0:
        lines[center_line] = "(CenterOfRotationPoint " + str(cx) + " " + str(cy) + ")\n"
    if initial_transform_line > 0:
        lines[initial_transform_line] = "(InitialTransformParametersFileName ./" + os.path.split(lines[initial_transform_line].split(" ")[1].split(")")[0])[1] + ")\n"        
    if size_line > 0:
        lines[size_line] = "(Size " + str(reg_level_dims[0]) + " " + str(reg_level_dims[1]) + ")\n"
    f.close()
    f = open(pth, 'w')
    f.writelines(lines)
    f.close()
    
def template_matching(f_img, m_img, stepsizes):
    logger = logging.getLogger('perform_registration')
    step_ta, step_tx, step_ty = stepsizes
    best_ta = 0
    best_tx = 0
    best_ty = 0
    best_sub_img = None
    best_ssd = (f_img**2).sum() + (m_img**2).sum()
    
    max_row_size = int(np.ceil(abs(f_img.shape[1] * np.sin(np.pi/4)) + abs(f_img.shape[0] * np.cos(np.pi/4))))
    max_col_size = int(np.ceil(abs(f_img.shape[1] * np.cos(np.pi/4)) + abs(f_img.shape[0] * np.sin(np.pi/4))))
    max_size = max_row_size if max_row_size > max_col_size else max_col_size
    padC = int(np.ceil((max_size - f_img.shape[1])/2.))
    padR = int(np.ceil((max_size - f_img.shape[0])/2.))
    f_img_pad = np.pad(f_img, ((padR, padR),(padC, padC)), 'constant')
    for ta in np.arange(0, 360, step_ta):
        f_img_rot = rotate(f_img_pad, ta, False)
        logger.debug("At rotation angle " + str(ta) + " of 360.")
        for tx in np.arange(-f_img_pad.shape[1], 2*f_img_pad.shape[1], step_tx):
            if tx > f_img_pad.shape[1]:
                continue
            local_min_pad_x = 0
            local_max_pad_x = 0
            sx = tx
            if sx < 0:
                local_min_pad_x = np.abs(tx)
                sx = 0
            ex = tx+m_img.shape[1]
            if ex < 0:
                continue
            if ex > f_img_rot.shape[1]:
                local_max_pad_x = ex - f_img_rot.shape[1]
            for ty in np.arange(-f_img_pad.shape[0], 2*f_img_pad.shape[0] , step_ty):
                if ty > f_img_pad.shape[0]:
                    continue
                local_min_pad_y = 0
                local_max_pad_y = 0
                sy = ty
                if sy < 0:
                    local_min_pad_y = np.abs(ty)
                    sy = 0
                ey = ty + m_img.shape[0]
                if ey < 0:
                    continue
                if ey > f_img_rot.shape[0]:
                    local_max_pad_y = ey - f_img_rot.shape[0]
                sub_img = f_img_rot[sy:ey, sx:ex]
                sub_img_padded = np.pad(sub_img, ((local_min_pad_y, local_max_pad_y), (local_min_pad_x, local_max_pad_x)), 'constant')
                ssd = ((sub_img_padded - m_img)**2).sum()
                if ssd < best_ssd:
                    best_tx = tx
                    best_ty = ty
                    best_ta = ta
                    best_ssd = ssd
                    best_sub_img = sub_img_padded
    if padC > 0:
      best_tx -= padC
    if padR > 0:
      best_ty -= padR
    return best_tx, best_ty, best_ta, best_sub_img

f_img = r"X:\Scans\TIGA\Projekte\Reddies\Etablierung\Trainingsset\CD20\1NBVOB_CD20_150_ER1_20 - 2015-12-04 15.07.14.ndpi"
m_img = r"X:\Scans\TIGA\Projekte\Reddies\Etablierung\Trainingsset\cMyc\1NBVOB_cMyc_500_ER2_20 - 2015-12-04 14.13.21.ndpi"
perform_registration(f_img, m_img, r"D:\tmp\test", debug=True)
apply_transformation_to_slide(m_img, r"D:\tmp\test\reg_result.tif", os.path.join(r"D:\tmp\test", "Parameters_Euler_filled.txt"), os.path.join(r"D:\tmp\test", "TransformParameters.0.txt"))